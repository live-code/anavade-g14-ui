import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemoUi1Component } from './demo-ui1.component';

const routes: Routes = [{ path: '', component: DemoUi1Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoUi1RoutingModule { }
