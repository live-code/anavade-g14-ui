import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoUi1RoutingModule } from './demo-ui1-routing.module';
import { DemoUi1Component } from './demo-ui1.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    DemoUi1Component
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    DemoUi1RoutingModule
  ]
})
export class DemoUi1Module { }
