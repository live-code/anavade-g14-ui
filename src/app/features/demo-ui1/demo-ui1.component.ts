import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, mergeMap } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Meteo } from '../../model/meteo';


@Component({
  selector: 'ava-demo-ui1',
  template: `
      <input type="text" [formControl]="inputCity" placeholder="Search meteo city">
      <ava-weather [city]="city" [color]="color"></ava-weather>
      <button (click)="color = 'blue'">blue</button>
      <button (click)="color = 'purple'">purple</button>

      <div style="width: 200px">
        <ava-chart-js [data]="temps"></ava-chart-js>
        <ava-chart2-js [data]="temps"></ava-chart2-js>
        <button (click)="temps = [1, 3, 5, 7, 12, 15]">temps 1</button>
        <button (click)="temps = [51, 43, 35, 27, 12, 5]">temps 2</button>
      </div>
  `,
  styles: [
  ]
})
export class DemoUi1Component implements OnDestroy {
  inputCity = new FormControl();
  city: string;
  color = 'red';
  temps = [10, 30, 3, 4, 10, 12];

  constructor(private http: HttpClient) {
    this.inputCity.valueChanges
      .pipe(
        filter(text => text.length > 2),
        debounceTime(1000),
        distinctUntilChanged(),
        // mergeMap(text => this.http.get<Meteo>(`http://api.openweathermap.org/data/2.5/weather?q=${text}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`))
      )
      .subscribe(text => {
        this.city = text;
      });

    // this.inputCity.setValue('Milano')
  }

  ngOnDestroy(): void {
    console.log('destroy')
  }
}
/*
ngOnChanges
ngOnInit
ngAfterContentInit
ngAfterViewInit
ngOnDestroy
*/
