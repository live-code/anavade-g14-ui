import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemoUi2Component } from './demo-ui2.component';

const routes: Routes = [{ path: '', component: DemoUi2Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoUi2RoutingModule { }
