import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ava-demo-ui2',
  template: `
    <ava-accordion [showOne]="true">
      <ava-accordion-panel title="Chapter 1">
        bla bla
      </ava-accordion-panel>
      <ava-accordion-panel
        headerBg="#purple"
        title="Chapter 2">
        <input type="text">
      </ava-accordion-panel>
      <ava-accordion-panel title="Chapter 3">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta eaque earum laudantium voluptas! Aliquid aperiam aspernatur doloremque ducimus ea eos fugit ipsum natus omnis, perferendis porro quaerat ullam ut velit?
      </ava-accordion-panel>
    </ava-accordion>
   
    <ava-google-map
      [coords]="coords"
      [zoom]="zoom"
    ></ava-google-map>
    
    <button (click)="coords = { lat: 44, lng: 14}">Coords 1</button>
    <button (click)="coords = { lat: 42, lng: 12}">Coords 2</button>
    <button (click)="zoom = zoom - 1">-</button>
    <button (click)="zoom = zoom + 1">+</button>
  `,
  styles: [
  ]
})
export class DemoUi2Component implements OnInit {
  coords = { lat: 43, lng: 13};
  zoom = 10;

  constructor() { }

  ngOnInit(): void {
  }

}
