import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoUi2RoutingModule } from './demo-ui2-routing.module';
import { DemoUi2Component } from './demo-ui2.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    DemoUi2Component
  ],
  imports: [
    CommonModule,
    DemoUi2RoutingModule,
    SharedModule
  ]
})
export class DemoUi2Module { }
