import { Component } from '@angular/core';

@Component({
  selector: 'ava-root',
  template: `
    <button routerLink="demo-ui1">Demo 1</button>
    <button routerLink="demo-ui2">Demo 2</button>
    <hr>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {
  title = 'avanade-g14-ui';
}
