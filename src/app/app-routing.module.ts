import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'demo-ui1', loadChildren: () => import('./features/demo-ui1/demo-ui1.module').then(m => m.DemoUi1Module) },
  { path: 'demo-ui2', loadChildren: () => import('./features/demo-ui2/demo-ui2.module').then(m => m.DemoUi2Module) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
