import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeatherComponent } from './components/weather.component';
import { ChartJsComponent } from './components/chart-js.component';
import { Chart2JsComponent } from './components/chart2-js.component';
import { GoogleMapComponent } from './components/google-map.component';
import { AccordionComponent } from './components/accordion.component';
import { AccordionGroupComponent } from './components/accordion-group.component';



@NgModule({
  declarations: [
    WeatherComponent,
    ChartJsComponent,
    Chart2JsComponent,
    GoogleMapComponent,
    AccordionComponent,
    AccordionGroupComponent
  ],
  exports: [
    WeatherComponent,
    ChartJsComponent,
    Chart2JsComponent,
    GoogleMapComponent,
    AccordionComponent,
    AccordionGroupComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
