import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'ava-accordion-panel',
  template: `
    <div class="card">
      <div 
        class="card-header"
        [style.background]="headerBg"
        (click)="toggle.emit()">{{title}}</div>
      <div class="card-body" *ngIf="isOpened">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: [
  ]
})
export class AccordionGroupComponent {
  @Input() title: string;
  @Input() isOpened = false;
  @Output() toggle = new EventEmitter<void>();
  @Input() headerBg: string;
}
