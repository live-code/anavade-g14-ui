import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Meteo } from '../../model/meteo';
import { mergeMap } from 'rxjs/operators';

/**
 * Component meteo
 */
@Component({
  selector: 'ava-weather',
  template: `
    <h1 [style.color]="color">{{city}}</h1>
    <div *ngIf="meteo">
      {{meteo.main.temp}} °
      <img [src]="'http://openweathermap.org/img/w/' + meteo?.weather[0].icon + '.png'" alt="">
      
      <div style="width: 300px">
        <ava-google-map
          [coords]="{ lat: meteo?.coord.lat, lng: meteo?.coord.lon}"
          [zoom]="5"
          height="150"
        ></ava-google-map>
      </div>
    </div>
  `,
})
export class WeatherComponent implements OnChanges {
  /**
   * City to display
   */
  @Input() city: string;
  /**
   * Title color
   */
  @Input() color: string;
  meteo: Meteo;

  constructor(private http: HttpClient) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.city && changes.city.currentValue) {
      this.http.get<Meteo>(`http://api.openweathermap.org/data/2.5/weather?q=${changes.city.currentValue}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
        .subscribe(val => this.meteo = val);
    }

    if (changes.color) {
      // ...
    }
  }


}
