import { AfterContentInit, Component, ContentChildren, Input, OnDestroy, OnInit, QueryList } from '@angular/core';
import { AccordionGroupComponent } from './accordion-group.component';

@Component({
  selector: 'ava-accordion',
  template: `
    <div class="accordion">
      <ng-content></ng-content>
    </div>
    
  `,
  styles: [`
    .accordion {
      border: 1px solid black;
      padding: 10px;
      border-radius: 10px;
    }
  `]
})
export class AccordionComponent implements OnDestroy, AfterContentInit {
  @ContentChildren(AccordionGroupComponent) groups: QueryList<AccordionGroupComponent>
  @Input() showOne = true;

  ngAfterContentInit(): void {
    const groups = this.groups.toArray();
    if (groups.length) {
      groups[0].isOpened = true;
    } else {
      throw new Error('Accordion must have children')
    }

    groups.forEach(g => {
      g.toggle
        .subscribe(() => {
          if (this.showOne) {
            this.closeAll();
            g.isOpened = true;
          } else {
            g.isOpened = !g.isOpened;
          }

        });
    });
  }

  closeAll(): void {
    this.groups.toArray().forEach(g => {
      g.isOpened = false;
    });
  }

  ngOnDestroy(): void {
    // TODO: unsubscribes
  }

}
