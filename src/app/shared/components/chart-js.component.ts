import { AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { BarController, BarElement, CategoryScale, Chart, LinearScale } from 'chart.js';
import { getChartConfig } from './chart-js.utils';

Chart.register(LinearScale, BarController, CategoryScale, BarElement);

@Component({
  selector: 'ava-chart-js',
  template: `
    <canvas id="myChart" width="400" height="400" #host></canvas>
  `,
  styles: [
  ]
})
export class ChartJsComponent implements AfterViewInit, OnChanges {
  @ViewChild('host') host: ElementRef<HTMLCanvasElement>;
  @Input() data: number[];
  myChart: Chart;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.data && !changes.data.firstChange) {
      this.myChart.data.datasets[0].data = changes.data.currentValue;
      this.myChart.update();
    }
  }

  ngAfterViewInit(): void {
    const ctx = this.host.nativeElement.getContext('2d');
    const config = Object.assign({}, getChartConfig(this.data))
    this.myChart = new Chart(ctx, config);
  }

}
