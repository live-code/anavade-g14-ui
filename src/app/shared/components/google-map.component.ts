import { AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';

interface Coords {
  lat: number;
  lng: number;
}
@Component({
  selector: 'ava-google-map',
  template: `
    <div #host style="width: 100%" [style.height.px]="height"></div>
  `,
  styles: [
  ]
})
export class GoogleMapComponent implements OnChanges {
  @ViewChild('host', { static: true }) el: ElementRef<HTMLElement>;
  @Input() coords: Coords;
  @Input() zoom: number;
  @Input() height = 300;
  map: google.maps.Map;
  marker: google.maps.Marker;

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.map) {
      this.init();
    }
    if (changes.coords && changes.coords.currentValue) {
      const coords: google.maps.LatLngLiteral = this.coords;
      if (this.marker) {
        //  this.marker.setMap(null);
        this.marker.setPosition(coords)
      } else {
        this.marker = new google.maps.Marker({
          position: coords,
          map: this.map,
          title: 'Where I am!',
        });
      }
      this.map.panTo(coords)
    }

    if (changes.zoom && changes.zoom.currentValue) {
      this.map.setZoom(changes.zoom.currentValue)
    }
    console.log(changes)
  }

  init(): void {
    this.map = new google.maps.Map(this.el.nativeElement, {
      // center: { lat: -34.397, lng: 150.644 },
      zoom: 7,
    });
  }

}
