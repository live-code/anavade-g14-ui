import { AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { BarController, BarElement, CategoryScale, Chart, LinearScale } from 'chart.js';
import { getChartConfig } from './chart-js.utils';

Chart.register(LinearScale, BarController, CategoryScale, BarElement);

@Component({
  selector: 'ava-chart2-js',
  template: `
    <canvas id="myChart" width="400" height="400" #host></canvas>
    <div #div></div>
  `,
  styles: [
  ]
})
export class Chart2JsComponent implements OnChanges {
  @ViewChild('host', { static: true }) host: ElementRef<HTMLCanvasElement>;
  @ViewChild('div') div: ElementRef<HTMLDivElement>;
  @Input() data: number[];
  myChart: Chart;

  init(): void {
    const ctx = this.host.nativeElement.getContext('2d');
    const config = Object.assign({}, getChartConfig(this.data))
    this.myChart = new Chart(ctx, config);
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('changes')
    if (!this.myChart) {
      this.init();
    } else {
      this.myChart.data.datasets[0].data = changes.data.currentValue;
      this.myChart.update();
    }
  }

}
