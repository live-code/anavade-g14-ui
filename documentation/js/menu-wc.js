'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">avanade-g14-ui documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-2d647fe02d37ac5bd0b880b5f3a9a0f4"' : 'data-target="#xs-components-links-module-AppModule-2d647fe02d37ac5bd0b880b5f3a9a0f4"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-2d647fe02d37ac5bd0b880b5f3a9a0f4"' :
                                            'id="xs-components-links-module-AppModule-2d647fe02d37ac5bd0b880b5f3a9a0f4"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/DemoUi1Module.html" data-type="entity-link">DemoUi1Module</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-DemoUi1Module-7a6067249706f703ba3c64dd41feffb8"' : 'data-target="#xs-components-links-module-DemoUi1Module-7a6067249706f703ba3c64dd41feffb8"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DemoUi1Module-7a6067249706f703ba3c64dd41feffb8"' :
                                            'id="xs-components-links-module-DemoUi1Module-7a6067249706f703ba3c64dd41feffb8"' }>
                                            <li class="link">
                                                <a href="components/DemoUi1Component.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DemoUi1Component</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/DemoUi1RoutingModule.html" data-type="entity-link">DemoUi1RoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/DemoUi2Module.html" data-type="entity-link">DemoUi2Module</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-DemoUi2Module-3f0736e8fac2dee5128520c6104fed69"' : 'data-target="#xs-components-links-module-DemoUi2Module-3f0736e8fac2dee5128520c6104fed69"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DemoUi2Module-3f0736e8fac2dee5128520c6104fed69"' :
                                            'id="xs-components-links-module-DemoUi2Module-3f0736e8fac2dee5128520c6104fed69"' }>
                                            <li class="link">
                                                <a href="components/DemoUi2Component.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DemoUi2Component</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/DemoUi2RoutingModule.html" data-type="entity-link">DemoUi2RoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link">SharedModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SharedModule-6bc0066ac07de48aa2d69c0aeeb6f2c9"' : 'data-target="#xs-components-links-module-SharedModule-6bc0066ac07de48aa2d69c0aeeb6f2c9"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SharedModule-6bc0066ac07de48aa2d69c0aeeb6f2c9"' :
                                            'id="xs-components-links-module-SharedModule-6bc0066ac07de48aa2d69c0aeeb6f2c9"' }>
                                            <li class="link">
                                                <a href="components/AccordionComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AccordionComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AccordionGroupComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AccordionGroupComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/Chart2JsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">Chart2JsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ChartJsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ChartJsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GoogleMapComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">GoogleMapComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/WeatherComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">WeatherComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/Clouds.html" data-type="entity-link">Clouds</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Coord.html" data-type="entity-link">Coord</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Coords.html" data-type="entity-link">Coords</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Main.html" data-type="entity-link">Main</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Meteo.html" data-type="entity-link">Meteo</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Sys.html" data-type="entity-link">Sys</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Weather.html" data-type="entity-link">Weather</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Wind.html" data-type="entity-link">Wind</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});